package Filters;

import Utils.Helper;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class AuthFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);

        Boolean isAuthenticated = false;

        boolean sessionExists = session != null;
        boolean isUserAccessPage = request.getRequestURI().endsWith("/sign_in") || request.getRequestURI().endsWith("/sign_up");
        boolean isRoot = request.getRequestURI().endsWith("/home");
        boolean isStatic = request.getRequestURI().contains("/resources");

        if (sessionExists) {
            isAuthenticated = (Boolean) session.getAttribute("auth");

            if (isAuthenticated == null) {
                isAuthenticated = false;
            }
        }

        request.setAttribute("links", Helper.getLinksByAuth(isAuthenticated));

        if (isRoot || isStatic) {
            filterChain.doFilter(request, response);
        } else {
            if (isAuthenticated) {
                if (isUserAccessPage) {
                    response.sendRedirect("profile");
                } else {
                    filterChain.doFilter(request, response);
                }
            } else {
                if (isUserAccessPage) {
                    filterChain.doFilter(request, response);
                } else {
                    response.sendRedirect(request.getContextPath() + "/sign_in");
                }
            }
        }
    }

    @Override
    public void destroy() {

    }

}

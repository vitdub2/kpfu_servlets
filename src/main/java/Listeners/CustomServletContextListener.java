package Listeners;

import Models.Entities.Comment.CommentRepository;
import Models.Entities.Comment.CommentRepositoryImpl;
import Models.Entities.Cookie.CookieRepository;
import Models.Entities.Cookie.CookieRepositoryImpl;
import Models.Entities.File.FileInfoRepository;
import Models.Entities.File.FileInfoRepositoryImpl;
import Models.Entities.Message.MessageRepository;
import Models.Entities.Message.MessageRepositoryImpl;
import Models.Entities.Post.PostRepository;
import Models.Entities.Post.PostRepositoryImpl;
import Models.Entities.User.UserRepository;
import Models.Entities.User.UserRepositoryImpl;
import Services.CommentSerivce.CommentService;
import Services.CommentSerivce.CommentServiceImpl;
import Services.CookieService.CookieService;
import Services.CookieService.CookieServiceImpl;
import Services.FileService.FileService;
import Services.FileService.FileServiceImpl;
import Services.MessageService.MessageService;
import Services.MessageService.MessageServiceImpl;
import Services.PostService.PostService;
import Services.PostService.PostServiceImpl;
import Services.UserService.UserService;
import Services.UserService.UserServiceImpl;
import Utils.ContextEnum;
import Utils.Helper;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

@WebListener
public class CustomServletContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        context.setAttribute(ContextEnum.VALIDATOR.name(), validator);

        DataSource source = Helper.getDataSource();

        UserRepository userRepository = new UserRepositoryImpl(source);
        UserService userService = new UserServiceImpl(userRepository);

        CookieRepository cookieRepository = new CookieRepositoryImpl(source);
        CookieService cookieService = new CookieServiceImpl(cookieRepository);

        PostRepository postRepository = new PostRepositoryImpl(source);
        PostService postService = new PostServiceImpl(postRepository);

        CommentRepository commentRepository = new CommentRepositoryImpl(source);
        CommentService commentService = new CommentServiceImpl(commentRepository);

        FileInfoRepository fileInfoRepository = new FileInfoRepositoryImpl(source);
        FileService fileService = new FileServiceImpl(fileInfoRepository);

        MessageRepository messageRepository = new MessageRepositoryImpl(source);
        MessageService messageService = new MessageServiceImpl(messageRepository);

        context.setAttribute(ContextEnum.POST_SERVICE.name(), postService);
        context.setAttribute(ContextEnum.COMMENT_SERVICE.name(), commentService);
        context.setAttribute(ContextEnum.USER_SERVICE.name(), userService);
        context.setAttribute(ContextEnum.COOKIE_SERVICE.name(), cookieService);
        context.setAttribute(ContextEnum.FILE_SERVICE.name(), fileService);
        context.setAttribute(ContextEnum.MESSAGE_SERVICE.name(), messageService);
    }
}

package Utils;

public enum PropsEnum {
    DB_URL,
    DB_LOGIN,
    DB_PASSWORD,
    DB_DRIVER,
    FILES_FOLDER
}

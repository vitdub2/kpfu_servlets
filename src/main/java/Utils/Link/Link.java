package Utils.Link;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Link {
    private String href;
    private String label;
}

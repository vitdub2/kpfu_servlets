package Utils;

public enum ContextEnum {
    USER_SERVICE,
    COOKIE_SERVICE,
    POST_SERVICE,
    COMMENT_SERVICE,
    FILE_SERVICE,
    MESSAGE_SERVICE,
    VALIDATOR,
}

package Utils;

import Utils.Link.Link;
import org.postgresql.Driver;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

public class Helper {

    public static Map<String, String> getPropertiesMap() {
        TreeMap<String, String> result = new TreeMap<>();
        Properties property = new Properties();

        String propsFileName = "config.properties";
        try {
            InputStream fis = Helper.class.getClassLoader().getResourceAsStream(propsFileName);
            property.load(fis);

            for (PropsEnum propsName : PropsEnum.values()){
                String name = propsName.name();
                result.put(name, property.getProperty(name));
            }

        } catch (IOException e) {
            System.err.println("ОШИБКА: Файл свойств отсуствует!");
        }

        return result;
    }

    public static DataSource getDataSource() {
        DriverManagerDataSource source = new DriverManagerDataSource();
        Map<String, String> properties = getPropertiesMap();
        source.setDriverClassName(properties.get(PropsEnum.DB_DRIVER.name()));
        source.setUrl(properties.get(PropsEnum.DB_URL.name()));
        source.setUsername(properties.get(PropsEnum.DB_LOGIN.name()));
        source.setPassword(properties.get(PropsEnum.DB_PASSWORD.name()));
        return source;
    }

    public static List<Link> getLinksByAuth(boolean isAuth) {
        ArrayList<Link> links = new ArrayList<>();
        links.add(
            Link.builder()
            .href("home")
            .label("Home")
            .build()
        );
        if (isAuth) {
            links.add(
                Link.builder()
                .href("users")
                .label("Users")
                .build()
            );
            links.add(
                Link.builder()
                .href("profile")
                .label("Profile")
                .build()
            );
            links.add(
                Link.builder()
                .href("create_post")
                .label("Create post")
                .build()
            );
            links.add(
                Link.builder()
                .href("posts")
                .label("Posts")
                .build()
            );
            links.add(
                Link.builder()
                .href("subscriptions")
                .label("Subscriptions")
                .build()
            );
            links.add(
                Link.builder()
                .href("chats")
                .label("Chats")
                .build()
            );
            links.add(
                Link.builder()
                .href("sign_out")
                .label("Sign out")
                .build()
            );
        } else {
            links.add(
                Link.builder()
                .href("sign_up")
                .label("Sign up")
                .build()
            );
            links.add(
                Link.builder()
                .href("sign_in")
                .label("Sign in")
                .build()
            );
        }
        return links;
    }
}

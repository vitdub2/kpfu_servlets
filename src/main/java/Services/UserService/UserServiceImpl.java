package Services.UserService;

import Models.Forms.SignInForm.SignInForm;
import Models.Forms.SignUpForm.SignUpForm;
import Models.Entities.User.User;
import Models.Entities.User.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService {
    UserRepository repository;
    PasswordEncoder encoder;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
        encoder = new BCryptPasswordEncoder();
    }

    @Override
    public Optional<User> signUp(SignUpForm userForm) {
        Optional<User> user = repository.findByLogin(userForm.getLogin());
        if (user.isPresent()) {
            return Optional.empty();
        } else {
            User newUser = User.builder()
                    .first_name(userForm.getFirst_name())
                    .last_name(userForm.getLast_name())
                    .login(userForm.getLogin())
                    .username(userForm.getUsername())
                    .hashPassword(encoder.encode(userForm.getPassword()))
                    .build();
            Optional<Long> optionalId = repository.create(newUser);
            Long id = optionalId.orElse(0L);
            newUser.setId(id);
            return Optional.of(newUser);
        }
    }

    @Override
    public Optional<User> signIn(SignInForm userForm) {
        String login = userForm.getLogin();
        String password = userForm.getPassword();
        Optional<User> user = repository.findByLogin(login);
        if (user.isPresent() && encoder.matches(password, user.get().getHashPassword())){
            return user;
        } else {
            return Optional.empty();
        }
    }

    @Override
    public List<User> getUsers() {
        return repository.findAll();
    }

    @Override
    public Optional<User> getUserById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean isSubscribed(Long fromId, Long toId) {
        return repository.isSubscribed(fromId, toId);
    }

    @Override
    public void makeSubscription(Long currentId, Long subId) {
        repository.makeSubscription(currentId, subId);
    }

    @Override
    public void unsubscribe(Long currentId, Long subId) {
        repository.unsubscribe(currentId, subId);
    }

    @Override
    public List<User> getSubscriptions(Long currentId) {
        return repository.getSubscriptions(currentId);
    }
}

package Services.UserService;

import Models.Forms.SignInForm.SignInForm;
import Models.Forms.SignUpForm.SignUpForm;
import Models.Entities.User.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> signUp(SignUpForm userForm);
    Optional<User> signIn(SignInForm userForm);
    List<User> getUsers();
    Optional<User> getUserById(Long id);
    boolean isSubscribed(Long fromId, Long toId);
    void makeSubscription(Long currentId, Long subId);
    void unsubscribe(Long currentId, Long subId);
    List<User> getSubscriptions(Long currentId);
}

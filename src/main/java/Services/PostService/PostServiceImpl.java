package Services.PostService;

import Models.Entities.Post.Post;
import Models.Entities.Post.PostRepository;
import Models.Forms.AddPostForm.AddPostForm;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class PostServiceImpl implements PostService {
    private PostRepository repository;

    public PostServiceImpl(PostRepository repository) {
        this.repository = repository;
    }

    @Override
    public void addPost(AddPostForm form) {
        String uuid = UUID.randomUUID().toString();
        Post post = Post.builder()
                .uuid(uuid)
                .authorId(form.getAuthorId())
                .text(form.getText())
                .title(form.getTitle())
                .build();
        repository.save(post);
    }

    @Override
    public List<Post> getPostsById(Long currentId) {
        return repository.getPostsByAuthorId(currentId);
    }

    @Override
    public List<Post> getAvailablePosts(Long currentId) {
        return repository.getSubscriptionsPosts(currentId);
    }

    @Override
    public Optional<Post> getPostByUuid(String uuid) {
        return repository.getPostByUuid(uuid);
    }
}

package Services.PostService;

import Models.Entities.Post.Post;
import Models.Forms.AddPostForm.AddPostForm;

import java.util.List;
import java.util.Optional;

public interface PostService {
    void addPost(AddPostForm form);
    List<Post> getPostsById(Long currentId);
    List<Post> getAvailablePosts(Long currentId);
    Optional<Post> getPostByUuid(String uuid);
}

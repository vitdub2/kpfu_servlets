package Services.FileService;

import Models.Entities.File.FileInfo;
import Models.Entities.File.FileInfoRepository;
import Utils.Helper;
import Utils.PropsEnum;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public class FileServiceImpl implements FileService {
    private final String FILES_FOLDER_PATH = Helper.getPropertiesMap().get(PropsEnum.FILES_FOLDER.name());
    private final FileInfoRepository repository;

    public FileServiceImpl(FileInfoRepository repository) {
        this.repository = repository;
    }

    @Override
    public void uploadFile(FileInfo fileInfo, InputStream stream) {
        repository.save(fileInfo);
        try {
            Files.copy(stream, Paths.get(FILES_FOLDER_PATH + '/' + fileInfo.getStorageFilename() + "." + fileInfo.getType().split("/")[1]));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<FileInfo> getInfoById(Long id) {
        return repository.findById(id);
    }

    @Override
    public void downloadFile(FileInfo info, OutputStream stream) {
        try {
            Files.copy(Paths.get(FILES_FOLDER_PATH + '/' + info.getStorageFilename() + "." + info.getType().split("/")[1]), stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void registerAvatar(Long fileId, Long userId) {
        this.repository.registerAvatar(fileId, userId);
    }

    @Override
    public Optional<FileInfo> getAvatarByUserId(Long id) {
        return repository.getAvatarByUserId(id);
    }

    @Override
    public Optional<FileInfo> getFileInfoByStorageFilename(String storageFilename) {
        return this.repository.getFileInfoByStorageFilename(storageFilename);
    }
}

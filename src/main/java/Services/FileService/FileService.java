package Services.FileService;

import Models.Entities.File.FileInfo;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;

public interface FileService {
    void uploadFile(FileInfo fileInfo, InputStream stream);
    Optional<FileInfo> getInfoById(Long id);
    void downloadFile(FileInfo info, OutputStream stream);
    void registerAvatar(Long fileId, Long userId);
    Optional<FileInfo> getAvatarByUserId(Long id);
    Optional<FileInfo> getFileInfoByStorageFilename(String storageFilename);
}

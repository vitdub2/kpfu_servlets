package Services.CookieService;

import Models.Entities.Cookie.CookieModel;

public interface CookieService {
    void addCookie(CookieModel cookieModel);
}

package Services.CookieService;

import Models.Entities.Cookie.CookieModel;
import Models.Entities.Cookie.CookieRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class CookieServiceImpl implements CookieService {
    CookieRepository repository;
    PasswordEncoder encoder;

    public CookieServiceImpl(CookieRepository repository) {
        this.repository = repository;
        encoder = new BCryptPasswordEncoder();
    }

    @Override
    public void addCookie(CookieModel cookieModel) {
        repository.save(cookieModel);
    }
}

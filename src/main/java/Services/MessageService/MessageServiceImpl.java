package Services.MessageService;

import Models.Entities.Message.Message;
import Models.Entities.Message.MessageRepository;

import java.util.List;

public class MessageServiceImpl implements MessageService {
    private MessageRepository repository;

    public MessageServiceImpl(MessageRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Message> getChat(Long fromId, Long toId) {
        return repository.getChat(fromId, toId);
    }

    @Override
    public void readChat(Long fromId, Long toId) {
        repository.readChat(fromId, toId);
    }

    @Override
    public void addMessage(Message message) {
        repository.save(message);
    }
}

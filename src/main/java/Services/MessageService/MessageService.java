package Services.MessageService;

import Models.Entities.Message.Message;

import java.util.List;

public interface MessageService {
    List<Message> getChat(Long fromId, Long toId);
    void readChat(Long fromId, Long toId);
    void addMessage(Message message);
}

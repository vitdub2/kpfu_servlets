package Services.CommentSerivce;

import Models.Entities.Comment.Comment;
import Models.Forms.AddCommentForm.AddCommentForm;

import java.util.List;

public interface CommentService {
    void addComment(AddCommentForm form);
    List<Comment> getCommentsByPostUuid(String uuid);
}

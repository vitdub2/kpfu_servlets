package Services.CommentSerivce;

import Models.Entities.Comment.Comment;
import Models.Entities.Comment.CommentRepository;
import Models.Forms.AddCommentForm.AddCommentForm;

import java.util.List;

public class CommentServiceImpl implements CommentService {
    private final CommentRepository repository;

    public CommentServiceImpl(CommentRepository repository) {
        this.repository = repository;
    }


    @Override
    public void addComment(AddCommentForm form) {
        Comment comment = Comment.builder()
                .authorId(form.getAuthorId())
                .postUuid(form.getPostUuid())
                .text(form.getText())
                .build();
        repository.save(comment);
    }

    @Override
    public List<Comment> getCommentsByPostUuid(String uuid) {
        return repository.findCommentsByPostUuid(uuid);
    }
}

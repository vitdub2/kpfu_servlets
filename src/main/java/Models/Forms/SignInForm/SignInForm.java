package Models.Forms.SignInForm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignInForm {
    @NotEmpty(message = "Login must not be empty")
    private String login;

    @NotEmpty(message = "Password must not be empty")
    private String password;
}

package Models.Forms.AddCommentForm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddCommentForm {

    @NotEmpty(message = "Write your comment")
    private String text;

    private Long authorId;

    private String postUuid;
}

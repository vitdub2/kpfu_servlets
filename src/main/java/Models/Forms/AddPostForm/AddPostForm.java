package Models.Forms.AddPostForm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddPostForm {
    @NotEmpty(message = "Title must not be empty")
    private String title;

    @NotEmpty(message = "Write your post")
    private String text;

    private Long authorId;
}

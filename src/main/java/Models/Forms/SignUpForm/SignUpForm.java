package Models.Forms.SignUpForm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SignUpForm {
    @Size(min = 8, message = "Login must contain at least 8 symbols")
    private String login;

    @NotEmpty(message = "Username must not be empty")
    private String username;

    @NotEmpty(message = "First name must not be empty")
    private String first_name;

    @NotEmpty(message = "Last name must not be empty")
    private String last_name;

    @Size(min = 8, message = "Password must contain at least 8 symbols")
    private String password;
}

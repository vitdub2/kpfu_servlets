package Models.Entities.User;

import Models.CRUDRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CRUDRepository<User> {
    List<User> findAllByAge(Integer age);
    Optional<User> findByLogin(String login);
    Optional<Long> create(User user);
    void makeSubscription(Long currId, Long subId);
    void unsubscribe(Long currId, Long subId);
    List<User> getSubscriptions(Long currentId);
    boolean isSubscribed(Long fromId, Long toId);
}


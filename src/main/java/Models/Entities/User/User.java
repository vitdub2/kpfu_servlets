package Models.Entities.User;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class User {
    private Long id;
    private String login;
    private String first_name;
    private String last_name;
    private String username;
    private String hashPassword;
}

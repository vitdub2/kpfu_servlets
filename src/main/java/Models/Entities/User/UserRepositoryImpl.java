package Models.Entities.User;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.Optional;

public class UserRepositoryImpl implements UserRepository {
    private final JdbcTemplate template;
    private final SimpleJdbcInsert insert;

    private final static String ID_COLUMN = "id";
    private final static String FIRST_NAME_COLUMN = "first_name";
    private final static String LAST_NAME_COLUMN = "last_name";
    private final static String USERNAME_COLUMN = "username";
    private final static String LOGIN_COLUMN = "login";
    private final static String PASSWORD_COLUMN = "password";

    public UserRepositoryImpl(DataSource source) {
        this.insert = new SimpleJdbcInsert(source);
        this.template = new JdbcTemplate(source);
        this.insert.withTableName("users").usingGeneratedKeyColumns(ID_COLUMN);
    }

    private final RowMapper<User> userRowMapper = ((resultSet, i) -> User.builder()
            .id(resultSet.getLong(ID_COLUMN))
            .first_name(resultSet.getString(FIRST_NAME_COLUMN))
            .last_name(resultSet.getString(LAST_NAME_COLUMN))
            .login(resultSet.getString(LOGIN_COLUMN))
            .username(resultSet.getString(USERNAME_COLUMN))
            .hashPassword(resultSet.getString(PASSWORD_COLUMN))
            .build()
    );

    @Override
    public List<User> findAllByAge(Integer age) {
        return this.template.query("select * from users where age=?", userRowMapper, age);
    }

    @Override
    public Optional<User> findByLogin(String login) {
        List<User> userList = this.template.query("select * from users where login=?", userRowMapper, login);
        if (userList.size() == 0) return Optional.empty();
        return Optional.of(userList.get(0));
    }

    @Override
    public Optional<Long> create(User user) {
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("first_name", user.getFirst_name())
                .addValue("last_name", user.getLast_name())
                .addValue("username", user.getUsername())
                .addValue("login", user.getLogin())
                .addValue("password", user.getHashPassword());

        Number id = this.insert.executeAndReturnKey(params);
        return Optional.of(id.longValue());
    }

    @Override
    public void makeSubscription(Long currId, Long subId) {
        this.template.update("insert into subscriptions(from_id, to_id) values(?, ?)", currId, subId);
    }

    @Override
    public void unsubscribe(Long currId, Long subId) {
        this.template.update("delete from subscriptions where from_id=? and to_id=?", currId, subId);
    }

    @Override
    public List<User> getSubscriptions(Long currentId) {
        return this.template.query("select * from subscriptions inner join users u on u.id = subscriptions.to_id where from_id=?", userRowMapper, currentId);
    }

    @Override
    public boolean isSubscribed(Long fromId, Long toId) {
        Integer count = this.template.queryForObject("select count(from_id) from subscriptions where from_id=? and to_id=?", Integer.class, fromId, toId);
        return count > 0;
    }

    @Override
    public List<User> findAll() {
        return this.template.query("select * from users", userRowMapper);
    }

    @Override
    public Optional<User> findById(Long id) {
        User user = this.template.queryForObject("select * from users where id=?", userRowMapper, id);
        if (user == null) return Optional.empty();
        return Optional.of(user);
    }

    @Override
    public void save(User user) {
    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void remove(User entity) {

    }

    @Override
    public void removeById(Long id) {

    }
}

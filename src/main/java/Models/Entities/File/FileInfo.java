package Models.Entities.File;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FileInfo {
    private Long id;
    private String type;
    private String originFilename;
    private String storageFilename;
    private Long size;
}

package Models.Entities.File;

import Models.CRUDRepository;

import java.util.Optional;

public interface FileInfoRepository extends CRUDRepository<FileInfo> {
    void registerAvatar(Long fileId, Long userId);
    Optional<FileInfo> getAvatarByUserId(Long id);
    Optional<FileInfo> getFileInfoByStorageFilename(String storageFilename);
}

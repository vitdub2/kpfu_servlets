package Models.Entities.File;

import com.sun.rowset.internal.Row;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

public class FileInfoRepositoryImpl implements FileInfoRepository {
    private final JdbcTemplate template;

    private final static String ID_COLUMN = "id";
    private final static String TYPE_COLUMN = "type";
    private final static String ORIGIN_FILENAME_COLUMN = "origin_filename";
    private final static String STORAGE_FILENAME_COLUMN = "storage_filename";
    private final static String SIZE_COLUMN = "size";

    public FileInfoRepositoryImpl(DataSource source) {
        this.template = new JdbcTemplate(source);
    }

    private RowMapper<FileInfo> fileInfoRowMapper = ((resultSet, i) -> FileInfo.builder()
            .id(resultSet.getLong(ID_COLUMN))
            .originFilename(resultSet.getString(ORIGIN_FILENAME_COLUMN))
            .storageFilename(resultSet.getString(STORAGE_FILENAME_COLUMN))
            .size(resultSet.getLong(SIZE_COLUMN))
            .type(resultSet.getString(TYPE_COLUMN))
            .build()
    );

    @Override
    public List<FileInfo> findAll() {
        return this.template.query("select * from files", fileInfoRowMapper);
    }

    @Override
    public Optional<FileInfo> findById(Long id) {
        FileInfo info = this.template.queryForObject("select * from files where id=?", fileInfoRowMapper, id);
        if (info == null) return Optional.empty();
        return Optional.of(info);
    }

    @Override
    public void save(FileInfo entity) {
        this.template.update("insert into files(type, origin_filename, storage_filename, size) VALUES (?,?,?,?)",
                entity.getType(),
                entity.getOriginFilename(),
                entity.getStorageFilename(),
                entity.getSize()
        );
    }

    @Override
    public void update(FileInfo entity) {

    }

    @Override
    public void remove(FileInfo entity) {

    }

    @Override
    public void removeById(Long id) {

    }

    @Override
    public void registerAvatar(Long fileId, Long userId) {
        RowMapper<Long> idMapper = (((resultSet, i) -> resultSet.getLong("id")));
        List<Long> oldFileIdList = this.template.query("select files.id from files inner join avatars a on files.id = a.avatar_id inner join users u on u.id = a.user_id where a.user_id=?", idMapper, userId);
        this.template.update("delete from avatars where user_id=?", userId);
        if (oldFileIdList.size() > 0){
            this.template.update("delete from files where id=?", oldFileIdList.get(0));
        }
        this.template.update("insert into avatars(avatar_id, user_id) VALUES (?,?)", fileId, userId);
    }

    @Override
    public Optional<FileInfo> getAvatarByUserId(Long id) {
        List<FileInfo> infoList = this.template.query("select f.id, f.type, f.origin_filename, f.size, f.storage_filename from avatars inner join files f on f.id = avatars.avatar_id where user_id=?", fileInfoRowMapper, id);
        if (infoList.size() == 0) return Optional.empty();
        return Optional.of(infoList.get(0));
    }

    @Override
    public Optional<FileInfo> getFileInfoByStorageFilename(String storageFilename) {
        List<FileInfo> infoList = this.template.query("select * from files where storage_filename=?", fileInfoRowMapper, storageFilename);
        if (infoList.size() == 0) return Optional.empty();
        return Optional.of(infoList.get(0));
    }
}

package Models.Entities.Comment;

import Models.CRUDRepository;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends CRUDRepository<Comment> {
    List<Comment> findCommentsByPostUuid(String uuid);
}

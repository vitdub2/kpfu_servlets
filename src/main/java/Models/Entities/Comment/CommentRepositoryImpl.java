package Models.Entities.Comment;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

public class CommentRepositoryImpl implements CommentRepository {
    private final JdbcTemplate template;

    private final static String ID_COLUMN = "id";
    private final static String AUTHOR_ID_COLUMN = "author_id";
    private final static String TEXT_COLUMN = "text";
    private final static String CREATED_AT_COLUMN = "create_at";
    private final static String POST_UUID_COLUMN = "post_uuid";
    private final static String USERNAME_COLUMN = "username";

    public CommentRepositoryImpl(DataSource source) {
        this.template = new JdbcTemplate(source);
    }

    private final RowMapper<Comment> commentMapper = ((resultSet, i) -> Comment.builder()
            .id(resultSet.getLong(ID_COLUMN))
            .text(resultSet.getString(TEXT_COLUMN))
            .postUuid(resultSet.getString(POST_UUID_COLUMN))
            .authorId(resultSet.getLong(AUTHOR_ID_COLUMN))
            .createdAt(resultSet.getDate(CREATED_AT_COLUMN))
            .authorUsername(resultSet.getString(USERNAME_COLUMN))
            .build());

    @Override
    public List<Comment> findCommentsByPostUuid(String uuid) {
        return this.template.query("select * from comments inner join users on comments.author_id = users.id where post_uuid=?", commentMapper, uuid);
    }

    @Override
    public List<Comment> findAll() {
        return this.template.query("select * from comments inner join users on comments.author_id = users.id", commentMapper);
    }

    @Override
    public Optional<Comment> findById(Long id) {
        Comment comment = this.template.queryForObject("select * from comments inner join users on comments.author_id = users.id where comments.id=?", commentMapper, id);
        if (comment == null) return Optional.empty();
        return Optional.of(comment);
    }

    @Override
    public void save(Comment entity) {
        this.template.update("insert into comments(author_id, text, post_uuid) values(?, ?, ?)",
                entity.getAuthorId(),
                entity.getText(),
                entity.getPostUuid()
        );
    }

    @Override
    public void update(Comment entity) {

    }

    @Override
    public void remove(Comment entity) {

    }

    @Override
    public void removeById(Long id) {

    }
}

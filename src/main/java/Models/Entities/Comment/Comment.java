package Models.Entities.Comment;

import lombok.*;

import java.util.Date;

@Data
@Builder
@EqualsAndHashCode
@ToString
public class Comment {
    private Long id;
    private Long authorId;
    private String authorUsername;
    private String text;
    private Date createdAt;
    private String postUuid;
}

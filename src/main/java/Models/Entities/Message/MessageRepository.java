package Models.Entities.Message;

import Models.CRUDRepository;

import java.util.List;

public interface MessageRepository extends CRUDRepository<Message> {
    List<Message> getChat(Long fromId, Long toId);
    void readChat(Long fromId, Long toId);
}

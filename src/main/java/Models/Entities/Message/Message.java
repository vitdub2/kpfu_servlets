package Models.Entities.Message;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class Message {
    private Long id;
    private Long from_id;
    private Long to_id;
    private String author_username;
    private String text;
    private Date created_at;
    private boolean is_read;
}

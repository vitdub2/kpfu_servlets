package Models.Entities.Message;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

public class MessageRepositoryImpl implements MessageRepository {
    private final JdbcTemplate template;

    private final static String ID_COLUMN = "id";
    private final static String FROM_ID_COLUMN = "from_id";
    private final static String TO_ID_COLUMN = "to_id";
    private final static String TEXT_COLUMN = "text";
    private final static String IS_READ_COLUMN = "is_read";
    private final static String CREATED_AT_COLUMN = "created_at";
    private final static String USERNAME_COLUMN = "username";

    public MessageRepositoryImpl(DataSource source) {
        this.template = new JdbcTemplate(source);
    }

    private RowMapper<Message> messageRowMapper = (((resultSet, i) -> Message.builder()
            .id(resultSet.getLong(ID_COLUMN))
            .from_id(resultSet.getLong(FROM_ID_COLUMN))
            .to_id(resultSet.getLong(TO_ID_COLUMN))
            .created_at(resultSet.getTimestamp(CREATED_AT_COLUMN))
            .is_read(resultSet.getBoolean(IS_READ_COLUMN))
            .text(resultSet.getString(TEXT_COLUMN))
            .author_username(resultSet.getString(USERNAME_COLUMN))
            .build()
    ));

    @Override
    public List<Message> getChat(Long fromId, Long toId) {
        return this.template.query("select * from messages inner join users u on u.id = messages.from_id where (from_id=? and to_id=?) or (from_id=? and to_id=?)", messageRowMapper, fromId, toId, toId, fromId);
    }

    @Override
    public void readChat(Long fromId, Long toId) {
        this.template.update("update messages set is_read=true where from_id=? or to_id=?", fromId, toId);
    }

    @Override
    public List<Message> findAll() {
        return this.template.query("select * from messages inner join users u on u.id = messages.from_id", messageRowMapper);
    }

    @Override
    public Optional<Message> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public void save(Message entity) {
        this.template.update("insert into messages(from_id, to_id, text) values (?, ?, ?)",
          entity.getFrom_id(),
          entity.getTo_id(),
          entity.getText()
        );
    }

    @Override
    public void update(Message entity) {

    }

    @Override
    public void remove(Message entity) {

    }

    @Override
    public void removeById(Long id) {

    }
}

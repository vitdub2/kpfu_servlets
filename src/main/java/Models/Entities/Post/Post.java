package Models.Entities.Post;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;

@Data
@Builder
@ToString
@EqualsAndHashCode
public class Post {
    private Long id;
    private Long authorId;
    private String text;
    private String title;
    private Date createdAt;
    private String uuid;
}

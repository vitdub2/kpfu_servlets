package Models.Entities.Post;

import Models.Entities.Comment.Comment;
import Models.Entities.User.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PostRepositoryImpl implements PostRepository {
    private final JdbcTemplate template;

    private final static String ID_COLUMN = "id";
    private final static String UUID_COLUMN = "uuid";
    private final static String AUTHOR_ID_COLUMN = "author_id";
    private final static String TEXT_COLUMN = "text";
    private final static String TITLE_COLUMN = "title";
    private final static String CREATED_AT_COLUMN = "created_at";

    public PostRepositoryImpl(DataSource source) {
        this.template = new JdbcTemplate(source);
    }

    private final RowMapper<Post> postRowMapper = ((resultSet, i) -> Post.builder()
            .id(resultSet.getLong(ID_COLUMN))
            .title(resultSet.getString(TITLE_COLUMN))
            .text(resultSet.getString(TEXT_COLUMN))
            .uuid(resultSet.getString(UUID_COLUMN))
            .authorId(resultSet.getLong(AUTHOR_ID_COLUMN))
            .createdAt(resultSet.getDate(CREATED_AT_COLUMN))
            .build()
    );

    @Override
    public Optional<Post> getPostByUuid(String uuid) {
        Post post = this.template.queryForObject("select * from posts where uuid=?", postRowMapper, uuid);
        if (post == null) return Optional.empty();
        return Optional.of(post);
    }

    @Override
    public List<Post> getPostsByAuthorId(Long id) {
        return this.template.query("select * from posts where author_id=?", postRowMapper, id);
    }

    @Override
    public List<Post> getSubscriptionsPosts(Long currId) {
        return this.template.query("select * from subscriptions inner join posts on posts.author_id = to_id where from_id=?", postRowMapper, currId);
    }

    @Override
    public List<Post> findAll() {
        return this.template.query("select * from comments", postRowMapper);
    }

    @Override
    public Optional<Post> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public void save(Post entity) {
        this.template.update(
                "insert into posts(author_id, text, title, uuid) values(?, ?, ?, ?)",
                entity.getAuthorId(),
                entity.getText(),
                entity.getTitle(),
                entity.getUuid()
        );
    }

    @Override
    public void update(Post entity) {

    }

    @Override
    public void remove(Post entity) {

    }

    @Override
    public void removeById(Long id) {

    }
}

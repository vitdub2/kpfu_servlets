package Models.Entities.Post;

import Models.CRUDRepository;

import java.util.List;
import java.util.Optional;

public interface PostRepository extends CRUDRepository<Post> {
    Optional<Post> getPostByUuid(String uuid);
    List<Post> getPostsByAuthorId(Long id);
    List<Post> getSubscriptionsPosts(Long currId);
}

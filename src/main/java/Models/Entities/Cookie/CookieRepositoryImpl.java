package Models.Entities.Cookie;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CookieRepositoryImpl implements CookieRepository {
    private DataSource source;

    public CookieRepositoryImpl(DataSource source) {
        this.source = source;
    }

    @Override
    public Optional<CookieModel> findByUserId(String userId) {
        return Optional.empty();
    }

    @Override
    public List<CookieModel> findAll() {
        return new ArrayList<>();
    }

    @Override
    public Optional<CookieModel> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public void save(CookieModel entity) {
        String selectSql = "select count(uuid) from cookies where user_id = ?";
        String updateSql = "update cookies set uuid = ? where user_id = ?";
        String insertSql = "insert into cookies(uuid, user_id) values(?, ?)";
        try {
            PreparedStatement selectStatement = this.source.getConnection().prepareStatement(selectSql);
            selectStatement.setLong(1, entity.getUserId());
            ResultSet set = selectStatement.executeQuery();
            while (set.next()) {
                int count = set.getInt("count");
                if (count > 0) {
                    PreparedStatement updateStatement = this.source.getConnection().prepareStatement(updateSql);
                    updateStatement.setString(1, entity.getUUID());
                    updateStatement.setLong(2, entity.getUserId());
                    updateStatement.execute();
                } else {
                    PreparedStatement statement = this.source.getConnection().prepareStatement(insertSql);
                    statement.setString(1, entity.getUUID());
                    statement.setLong(2, entity.getUserId());
                    statement.execute();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public void update(CookieModel entity) {

    }

    @Override
    public void remove(CookieModel entity) {

    }

    @Override
    public void removeById(Long id) {

    }
}

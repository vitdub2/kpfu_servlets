package Models.Entities.Cookie;

import Models.CRUDRepository;

import java.util.Optional;

public interface CookieRepository extends CRUDRepository<CookieModel> {
    Optional<CookieModel> findByUserId(String userId);
}

package Models.Entities.Cookie;

import lombok.*;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
public class CookieModel {
    private Long userId;
    private String UUID;
}

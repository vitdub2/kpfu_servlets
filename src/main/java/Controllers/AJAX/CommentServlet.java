package Controllers.AJAX;

import Models.Entities.Comment.Comment;
import Models.Forms.AddCommentForm.AddCommentForm;
import Services.CommentSerivce.CommentService;
import Utils.ContextEnum;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@WebServlet(name = "CommentServlet", urlPatterns = "/ajax/comments")
public class CommentServlet extends HttpServlet {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private CommentService commentService;
    private Validator validator;


    @Override
    public void init() throws ServletException {
        commentService = (CommentService) this.getServletContext().getAttribute(ContextEnum.COMMENT_SERVICE.name());
        validator = (Validator) this.getServletContext().getAttribute(ContextEnum.VALIDATOR.name());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uuid = req.getParameter("uuid");
        List<Comment> comments = commentService.getCommentsByPostUuid(uuid);

        resp.setContentType("application/json");
        resp.getWriter().println(objectMapper.writeValueAsString(comments));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long currId = (Long) req.getSession().getAttribute("user_id");

        AddCommentForm form = objectMapper.readValue(req.getReader(), AddCommentForm.class);
        form.setAuthorId(currId);

        Set<ConstraintViolation<AddCommentForm>> errors = validator.validate(form);
        List<String> errList = errors.stream().map(ConstraintViolation::getMessage).collect(Collectors.toList());
        if (errors.size() == 0) {
            commentService.addComment(form);
        }

        resp.setContentType("application/json");
        resp.getWriter().println(objectMapper.writeValueAsString(errList));
    }
}

package Controllers.AJAX;

import Services.UserService.UserService;
import Utils.ContextEnum;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "SubscriptionServlet", urlPatterns = "/ajax/subscription")
public class SubscriptionServlet extends HttpServlet {
    private UserService userService;
    private final ObjectMapper objectMapper = new ObjectMapper();


    @Override
    public void init() throws ServletException {
        userService = (UserService) this.getServletContext().getAttribute(ContextEnum.USER_SERVICE.name());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long subId = Long.valueOf(req.getParameter("id"));
        Long currId = (Long) req.getSession().getAttribute("user_id");
        boolean isSubscribed = userService.isSubscribed(currId,subId);
        if (isSubscribed) {
            userService.unsubscribe(currId, subId);
        } else {
            userService.makeSubscription(currId, subId);
        }

        resp.setContentType("application/json");
        resp.getWriter().println(objectMapper.writeValueAsString(!isSubscribed));
    }
}

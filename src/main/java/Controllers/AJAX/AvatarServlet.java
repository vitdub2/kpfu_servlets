package Controllers.AJAX;

import Models.Entities.File.FileInfo;
import Services.FileService.FileService;
import Utils.ContextEnum;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@MultipartConfig
@WebServlet(name = "AvatarServlet", urlPatterns = "/ajax/avatar")
public class AvatarServlet extends HttpServlet {
    private FileService fileService;

    @Override
    public void init() throws ServletException {
        fileService = (FileService) this.getServletContext().getAttribute(ContextEnum.FILE_SERVICE.name());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long userId = Long.valueOf(req.getParameter("id"));
        Optional<FileInfo> optionalFileInfo = fileService.getAvatarByUserId(userId);
        if (optionalFileInfo.isPresent()){
            FileInfo info = optionalFileInfo.get();
            resp.setContentType(info.getType());
            resp.setContentLength(info.getSize().intValue());
            resp.setHeader("Content-Disposition", "filename=\"" + info.getOriginFilename() + "\"");
            fileService.downloadFile(info, resp.getOutputStream());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part part = req.getPart("file");
        FileInfo fileInfo = FileInfo.builder()
                .type(part.getContentType())
                .size(part.getSize())
                .originFilename(part.getSubmittedFileName())
                .storageFilename(UUID.randomUUID().toString())
                .build();

        fileService.uploadFile(fileInfo, part.getInputStream());
        FileInfo info = fileService.getFileInfoByStorageFilename(fileInfo.getStorageFilename()).get();
        Long userId = (Long) req.getSession().getAttribute("user_id");
        fileService.registerAvatar(info.getId(), userId);
        //resp.sendRedirect("profile");
    }
}

package Controllers;

import Models.Entities.Message.Message;
import Models.Entities.User.User;
import Services.MessageService.MessageService;
import Services.UserService.UserService;
import Utils.ContextEnum;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "ChatsServlet", urlPatterns = "/chats")
public class ChatsServlet extends HttpServlet {
    private MessageService messageService;
    private UserService userService;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void init() throws ServletException {
        messageService = (MessageService) this.getServletContext().getAttribute(ContextEnum.MESSAGE_SERVICE.name());
        userService = (UserService) this.getServletContext().getAttribute(ContextEnum.USER_SERVICE.name());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String toIdRaw = req.getParameter("toId");
        Long currId = (Long) req.getSession().getAttribute("user_id");
        List<User> users = userService.getUsers();
        users = users.stream().filter(user -> !user.getId().equals(currId)).collect(Collectors.toList());
        req.setAttribute("users", users);
        req.setAttribute("chat", null);
        if (toIdRaw != null) {
            Long toId = Long.valueOf(toIdRaw);
            List<Message> chat = messageService.getChat(currId, toId);
            req.setAttribute("chat", chat);
        }
        req.getRequestDispatcher("/pages/chat_page.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Long currId = (Long) req.getSession().getAttribute("user_id");
        Long toId = Long.valueOf(req.getParameter("toId"));
        String text = objectMapper.readValue(req.getReader(), String.class);
        Message newMessage = Message.builder()
                .from_id(currId)
                .to_id(toId)
                .text(text)
                .build();
        this.messageService.addMessage(newMessage);

        List<Message> chat = messageService.getChat(currId, toId);

        resp.setContentType("application/json");
        resp.getWriter().println(objectMapper.writeValueAsString(chat));
    }
}

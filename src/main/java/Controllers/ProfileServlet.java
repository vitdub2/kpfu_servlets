package Controllers;

import Models.Entities.File.FileInfo;
import Models.Entities.Post.Post;
import Models.Entities.User.User;
import Services.FileService.FileService;
import Services.PostService.PostService;
import Services.UserService.UserService;
import Utils.ContextEnum;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "ProfileServlet", urlPatterns = "/profile")
public class ProfileServlet extends HttpServlet {
    private UserService userService;
    private PostService postService;
    private FileService fileService;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext context = this.getServletContext();
        userService = (UserService) context.getAttribute(ContextEnum.USER_SERVICE.name());
        postService = (PostService) context.getAttribute(ContextEnum.POST_SERVICE.name());
        fileService = (FileService) context.getAttribute(ContextEnum.FILE_SERVICE.name());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Long id = (Long) session.getAttribute("user_id");
        User user = userService.getUserById(id).get();
        List<Post> posts = postService.getPostsById(id);
        Optional<FileInfo> avatar = fileService.getAvatarByUserId(id);
        try {
            req.setAttribute("my_posts", posts);
            req.setAttribute("user", user);
            req.setAttribute("avatar", avatar.isPresent());
            req.getRequestDispatcher("/pages/profile.jsp").forward(req, resp);
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

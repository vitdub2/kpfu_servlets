package Controllers.User;

import Models.Entities.User.User;
import Services.UserService.UserService;
import Utils.ContextEnum;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "UsersServlet", urlPatterns = "/users")
public class UsersServlet extends HttpServlet {
    private UserService service;

    @Override
    public void init() throws ServletException {
        super.init();
        this.service = (UserService) this.getServletContext().getAttribute(ContextEnum.USER_SERVICE.name());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<User> users = this.service.getUsers();
        Long authId = (Long) req.getSession().getAttribute("user_id");
        users = users.stream().filter(user -> !user.getId().equals(authId)).collect(Collectors.toList());
        req.setAttribute("users", users);
        try {
            req.getRequestDispatcher("/pages/users.jsp").forward(req, resp);
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }
}

package Controllers.User;

import Models.Entities.Post.Post;
import Models.Entities.User.User;
import Services.PostService.PostService;
import Services.UserService.UserService;
import Utils.ContextEnum;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "UserServlet", urlPatterns = "/user/*")
public class UserServlet extends HttpServlet {
    private UserService userService;
    private PostService postService;

    @Override
    public void init() throws ServletException {
        super.init();
        this.userService = (UserService) this.getServletContext().getAttribute(ContextEnum.USER_SERVICE.name());
        this.postService = (PostService) this.getServletContext().getAttribute(ContextEnum.POST_SERVICE.name());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String path = req.getPathInfo();
        String[] splitted = path.split("/");
        Long user_id = Long.valueOf(splitted[1]);
        Long auth_id = (Long) req.getSession().getAttribute("user_id");
        Optional<User> user = userService.getUserById(user_id);
        List<Post> posts = postService.getPostsById(user_id);
        boolean subscribed = userService.isSubscribed(auth_id, user_id);
        if (user_id.equals(auth_id)) {
            resp.sendRedirect(req.getContextPath() + "/profile");
        } else if (user.isPresent()) {
            req.setAttribute("subscribed", subscribed);
            req.setAttribute("user", user.get());
            req.setAttribute("posts", posts);
            req.getRequestDispatcher("/pages/user_page.jsp").forward(req, resp);
        }
    }
}


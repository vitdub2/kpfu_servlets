package Controllers.User;

import Models.Entities.User.User;
import Services.UserService.UserService;
import Utils.ContextEnum;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SubscriptionsServlet", urlPatterns = "/subscriptions")
public class SubscriptionsServlet extends HttpServlet {
    private UserService userService;

    @Override
    public void init() throws ServletException {
        super.init();
        this.userService = (UserService) this.getServletContext().getAttribute(ContextEnum.USER_SERVICE.name());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long user_id = (Long) req.getSession().getAttribute("user_id");
        List<User> subs = userService.getSubscriptions(user_id);
        req.setAttribute("subs", subs);
        req.getRequestDispatcher("/pages/subscriptions.jsp").forward(req, resp);
    }
}

package Controllers.Post;

import Models.Entities.Comment.Comment;
import Models.Entities.Post.Post;
import Models.Forms.AddCommentForm.AddCommentForm;
import Services.CommentSerivce.CommentService;
import Services.PostService.PostService;
import Utils.ContextEnum;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "PostServlet", urlPatterns = "/post/*")
public class PostServlet extends HttpServlet {
    private PostService postService;
    private CommentService commentService;

    @Override
    public void init() throws ServletException {
        ServletContext ctx = this.getServletContext();
        postService = (PostService) ctx.getAttribute(ContextEnum.POST_SERVICE.name());
        commentService = (CommentService) ctx.getAttribute(ContextEnum.COMMENT_SERVICE.name());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getPathInfo();
        String[] splitted = path.split("/");
        String uuid = splitted[1];
        Optional<Post> post = postService.getPostByUuid(uuid);
        List<Comment> comments = commentService.getCommentsByPostUuid(uuid);
        if (post.isPresent()){
            req.setAttribute("comments", comments);
            req.setAttribute("post", post.get());
            req.getRequestDispatcher("/pages/post_page.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = (Long) req.getSession().getAttribute("user_id");
        String path = req.getPathInfo();
        String[] splitted = path.split("/");
        String uuid = splitted[1];
        commentService.addComment(AddCommentForm.builder()
                .authorId(id)
                .postUuid(uuid)
                .text(req.getParameter("text"))
                .build()
        );
        resp.sendRedirect(uuid);
    }
}

package Controllers.Post;

import Models.Forms.AddPostForm.AddPostForm;
import Services.PostService.PostService;
import Utils.ContextEnum;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@WebServlet(name = "CreatePostServlet", urlPatterns = "/create_post")
public class CreatePostServlet extends HttpServlet {
    private PostService service;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private Validator validator;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext ctx = this.getServletContext();
        service = (PostService) ctx.getAttribute(ContextEnum.POST_SERVICE.name());
        validator = (Validator) ctx.getAttribute(ContextEnum.VALIDATOR.name());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/pages/create_post.jsp").forward(req, resp);
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Long id = (Long) session.getAttribute("user_id");
        AddPostForm form = objectMapper.readValue(req.getReader(), AddPostForm.class);
        form.setAuthorId(id);
        Set<ConstraintViolation<AddPostForm>> errors = validator.validate(form);
        Map<String, String> errorMap = errors.stream().collect(Collectors.toMap(err -> err.getPropertyPath().toString(), ConstraintViolation::getMessage));
        if (errors.size() == 0) {
            service.addPost(form);
        }
        resp.setContentType("application/json");
        resp.getWriter().println(objectMapper.writeValueAsString(errorMap));
    }
}

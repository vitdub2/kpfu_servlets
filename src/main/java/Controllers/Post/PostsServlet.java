package Controllers.Post;

import Models.Entities.Post.Post;
import Services.PostService.PostService;
import Utils.ContextEnum;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "PostsServlet", urlPatterns = "/posts")
public class PostsServlet extends HttpServlet {
    private PostService service;

    @Override
    public void init() throws ServletException {
        ServletContext ctx = this.getServletContext();
        service = (PostService) ctx.getAttribute(ContextEnum.POST_SERVICE.name());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Long id = (Long) session.getAttribute("user_id");
        List<Post> myPosts = service.getPostsById(id);
        List<Post> anotherPosts = service.getAvailablePosts(id);
        req.setAttribute("my_posts", myPosts);
        req.setAttribute("posts", anotherPosts);
        req.getRequestDispatcher("/pages/posts_page.jsp").forward(req, resp);
    }
}

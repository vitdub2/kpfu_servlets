package Controllers.Auth;

import Models.Forms.SignUpForm.SignUpForm;
import Models.Entities.User.User;
import Services.UserService.UserService;
import Utils.ContextEnum;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@WebServlet(name = "SignUpServlet", urlPatterns = "/sign_up")
public class SignUpServlet extends HttpServlet {
    private UserService service;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private Validator validator;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext context = this.getServletContext();
        service = (UserService) context.getAttribute(ContextEnum.USER_SERVICE.name());
        validator = (Validator) context.getAttribute(ContextEnum.VALIDATOR.name());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/pages/sign_up.jsp").forward(req, resp);
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        SignUpForm form = objectMapper.readValue(req.getReader(), SignUpForm.class);
        Set<ConstraintViolation<SignUpForm>> errorSet = validator.validate(form);
        Map<String, String> errorMap = errorSet.stream().collect(Collectors.toMap(err -> err.getPropertyPath().toString(), ConstraintViolation::getMessage));

        if (errorMap.size() == 0) {
            Optional<User> user = service.signUp(form);
            if (user.isPresent()) {
                HttpSession session = req.getSession();
                session.setAttribute("auth", true);
                session.setAttribute("user_id", user.get().getId());
            } else {
                errorMap.put("login", "This login is already occupied");
            }
        }
        resp.setContentType("application/json");
        resp.getWriter().println(objectMapper.writeValueAsString(errorMap));
    }
}

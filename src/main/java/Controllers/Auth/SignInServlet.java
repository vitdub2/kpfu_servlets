package Controllers.Auth;

import Models.Entities.Cookie.CookieModel;
import Models.Forms.SignInForm.SignInForm;
import Models.Entities.User.User;
import Services.CookieService.CookieService;
import Services.UserService.UserService;
import Utils.ContextEnum;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@WebServlet(name = "SignInServlet", urlPatterns = "/sign_in")
public class SignInServlet extends HttpServlet {
    private UserService userService;
    //private CookieService cookieService;
    private Validator validator;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext context = this.getServletContext();
        userService = (UserService) context.getAttribute(ContextEnum.USER_SERVICE.name());
        validator = (Validator) context.getAttribute(ContextEnum.VALIDATOR.name());
        //cookieService = (CookieService) context.getAttribute(ContextEnum.COOKIE_SERVICE.name());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/pages/sign_in.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        SignInForm form = objectMapper.readValue(req.getReader(), SignInForm.class);
        Set<ConstraintViolation<SignInForm>> errors = validator.validate(form);
        List<String> errorsList = errors.stream().map(ConstraintViolation::getMessage).collect(Collectors.toList());
        resp.setContentType("application/json");
        if (errors.size() == 0) {
            Optional<User> signedUser = userService.signIn(form);
            if (signedUser.isPresent()) {
                UUID uuid = UUID.randomUUID();
                resp.addCookie(
                        new Cookie("Auth", uuid.toString())
                );
                resp.setHeader("Auth", uuid.toString());
                HttpSession session = req.getSession();
                session.setAttribute("auth", true);
                session.setAttribute("user_id", signedUser.get().getId());
//            cookieService.addCookie(
//                    CookieModel.builder()
//                            .UUID(uuid.toString())
//                            .userId(signedUser.get().getId())
//                            .build()
//            );
            } else {
                errorsList.add("Invalid login or password");
            }
        }
        resp.getWriter().println(objectMapper.writeValueAsString(errorsList));
    }
}

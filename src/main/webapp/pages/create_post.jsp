<html>
<head>
    <title>Create a post</title>
    <%@ include file="modules/head.jsp"%>
</head>
<body>
<%@ include file="modules/navbar.jsp" %>
<main>
    <div class="container">
        <div class="form-wrapper">
            <h2>Create a post</h2>
            <div class="form-group">
                <label for="title-input">Title</label>
                <input type="text" name="title" id="title-input" class="form-control">
                <div id="title-feedback" class="invalid-feedback"></div>
            </div>
            <div class="form-group">
                <label for="text-input">Text</label>
                <textarea name="text" id="text-input" class="form-control"></textarea>
                <div id="text-feedback" class="invalid-feedback"></div>
            </div>
            <button onclick="upload()" class="btn btn-primary" id="submit-btn">
                Submit
            </button>
        </div>
    </div>
</main>
</body>
<script>
    const renderInvalidForm = (errors) => {
        $('input').removeClass('is-invalid');
        $('.invalid-feedback').html('');
        const keys = Object.keys(errors);
        if (keys && keys.length > 0) {
            for (const key of keys) {
                $('#' + key + "-input").addClass('is-invalid');
                $('#' + key + "-feedback").html(errors[key]);
            }
        } else {
            window.location = '${pageContext.request.contextPath}/profile';
        }
    }

    const upload = () => {
        let data = {
            title: $("#title-input").val(),
            text: $("#text-input").val(),
        }
        $.ajax({
            url: "${pageContext.request.contextPath}/create_post",
            type: "POST",
            data: JSON.stringify(data),
            success: (resp) => {
                renderInvalidForm(resp);
            },
            cache: false,
            contentType: false,
            processData: false
        })
    }
</script>
</html>

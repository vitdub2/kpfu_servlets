<html>
<head>
    <title>Chats</title>
    <%@ include file="modules/head.jsp"%>
</head>
<body>
<%@ include file="modules/navbar.jsp" %>
<script>
    const renderChat = (messages) => {
        const wrapper = $("#chat");
        wrapper.html('')
        messages.forEach(message => {
            const date = new Date(message.created_at);
            const formatedDate = date.toLocaleDateString().split("/").reverse().join("-");
            wrapper.append(`
                <div class="message">
                    <h4>`+ message.author_username +`</h4>
                    <p>` + message.text + `</p>
                    <h6>` + formatedDate + `</h6>
                </div>
            `)
        })
    }

    const upload = () => {
        const val = $("#text").val();
        $.ajax({
            url: "",
            type: "POST",
            data: JSON.stringify(val),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: (resp) => {
                renderChat(resp);
            },
        })
    }
</script>
<main>
    <div class="container">
        <h2>Chats</h2>
        <div class="row">
            <div class="col-3">
                <h3>Users</h3>
                <c:forEach items="${users}" var="user">
                    <a href="chats?toId=${user.id}">
                        <h5>${user.username}</h5>
                    </a>
                </c:forEach>
            </div>
            <div class="col-9">
                <c:choose>
                    <c:when test="${chat != null}">
                        <div class="chat" id="chat">
                            <c:forEach items="${chat}" var="message">
                                <div class="message">
                                    <h4>${message.author_username}</h4>
                                    <p>${message.text}</p>
                                    <h6>${message.created_at}</h6>
                                </div>
                            </c:forEach>
                        </div>
                        <div>
                            <div class="form-group">
                                <textarea class="form-control" id="text" name="text"></textarea>
                            </div>
                            <button class="btn btn-primary" onclick="upload()">Send</button>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <p>Choose a user to get a chat with him</p>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</main>
</body>
</html>

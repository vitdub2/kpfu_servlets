<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
    <%@ include file="modules/head.jsp"%>
</head>
<body>
    <%@ include file="modules/navbar.jsp" %>
    <main>
        <div class="container">
            <h2>Users</h2>
            <ul class="list-group">
                <c:forEach items="${users}" var="user">
                    <a role="button" href="user/${user.id}" type="button" class="list-group-item list-group-item-action">
                        ${user.username}
                    </a>
                </c:forEach>
            </ul>
        </div>
    </main>
</body>
</html>

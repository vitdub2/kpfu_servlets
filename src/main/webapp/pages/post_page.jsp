<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${post.getTitle()}</title>
    <%@ include file="modules/head.jsp"%>
</head>
<body>
    <%@ include file="modules/navbar.jsp" %>
    <main>
        <div class="container">
            <h2>${post.getTitle()}</h2>
            <p>${post.getText()}</p>
            <div class="comments">
                <h3>Comments</h3>
                <div>
                    <h5>Add new comment</h5>
                    <div class="form-group">
                        <textarea name="text" id="text-input" placeholder="Your comment" class="form-control"></textarea>
                        <div id="text-feedback" class="invalid-feedback"></div>
                    </div>
                    <button class="btn btn-primary" id="submit-btn" onclick="sendComment()">
                        Submit
                    </button>
                </div>
                <div id="comments_block">
                    <c:forEach items="${comments}" var="comment">
                        <div class="comment">
                            <a href="${pageContext.request.contextPath}/user/${comment.authorId}">
                                <h4>${comment.authorUsername}</h4>
                            </a>
                            <h6>Created at: ${comment.createdAt}</h6>
                            <p>${comment.text}</p>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </main>
    <script>
        const sendComment = () => {
            const data = {
                postUuid: "${post.uuid}",
                text: $("#text-input").val(),
            }

            $.ajax({
                url: "${pageContext.request.contextPath}/ajax/comments",
                type: "POST",
                data: JSON.stringify(data),
                cache: false,
                contentType: false,
                processData: false,
                success: (errors) => {
                    renderForm(errors);
                    if (errors && errors.length === 0){
                        fetchComments();
                    }
                }
            })
        }

        const renderComments = (comments) => {
            const wrapper = $("#comments_block");
            const commentsElements = comments.map(comment => {
                const date = new Date(comment.createdAt);
                const formatedDate = date.toLocaleDateString().split("/").reverse().join("-") + " " + date.toLocaleTimeString();
                return (
                    `<div class="comment">
                        <a href="${pageContext.request.contextPath}/user/` + comment.authorId +`">
                            <h4>` + comment.authorUsername + `</h4>
                        </a>
                    <h6>Created at: ` + formatedDate + `</h6>
                    <p>` + comment.text +`</p>
                </div>`
                )
            });
            wrapper.html(commentsElements.toString().replaceAll(",", ""));
        }

        const fetchComments = () => {
            $.ajax({
                url: "${pageContext.request.contextPath}/ajax/comments?uuid=${post.uuid}",
                type: "GET",
                success: (resp) => {
                    renderComments(resp);
                }
            })
        }

        const renderForm = (errors) => {
            const error = errors[0];
            const input = $('#text-input');
            const feedback = $('#text-feedback');
            input.removeClass('is-invalid');
            feedback.html('');
            if (error) {
                input.addClass('is-invalid');
                feedback.html(error);
            }
        }
    </script>
</body>
</html>

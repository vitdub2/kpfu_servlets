<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
    <%@ include file="modules/head.jsp"%>
</head>
<script>
    const upload = () => {
        let formData = new FormData($("#form")[0]);
        $.ajax({
            url: "${pageContext.request.contextPath}/ajax/avatar",
            type: "POST",
            data: formData,
            success: () => {
                window.location.reload();
            },
            cache: false,
            contentType: false,
            processData: false
        })
    }
</script>
<body>
    <%@ include file="modules/navbar.jsp" %>
    <main>
        <div class="container">
            <h2>Profile</h2>
            <h4>Hello ${user.username}</h4>
            <%@ include file="modules/profile_avatar.jsp" %>
            <form id="form" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="file">Choose avatar</label>
                    <input type="file" name="file" class="form-control-file" id="file" onchange="upload()">
                </div>
            </form>
            <%@ include file="modules/my_posts.jsp" %>
        </div>
    </main>
</body>
</html>


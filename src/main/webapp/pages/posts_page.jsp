<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Posts</title>
    <%@ include file="modules/head.jsp"%>
</head>
<body>
<%@ include file="modules/navbar.jsp" %>
<main>
    <div class="container">
        <%@ include file="modules/my_posts.jsp" %>
        <div class="sub-posts">
            <c:choose>
                <c:when test="${posts.size() > 0}">
                    <div class="accordion" id="subPostsAccordion">
                        <div class="card">
                            <div class="card-header" id="subPostsHeading" role="button">
                                <div class="d-flex justify-content-between align-items-center" data-toggle="collapse" data-target="#subPosts" aria-expanded="false" aria-controls="subPosts">
                                    <h3>Subscriptions posts</h3>
                                    <h5 style="margin-right: 10px">
                                        <span class="badge badge-primary badge-pill">${posts.size()}</span>
                                    </h5>
                                </div>
                            </div>

                            <div id="subPosts" class="collapse show" aria-labelledby="subPostsHeading" data-parent="#subPostsAccordion">
                                <div class="list-group">
                                    <c:forEach items="${posts}" var="post">
                                        <a href="${pageContext.request.contextPath}/post/${post.getUuid()}" class="list-group-item list-group-item-action">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">${post.getTitle()}</h5>
                                                <small>${post.createdAt}</small>
                                            </div>
                                        </a>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <p>You don`t have any subscriptions</p>
                    <a href="users">It`s time to make some!</a>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</main>
</body>
</html>

<div class="my_posts">
    <c:choose>
        <c:when test="${my_posts.size() > 0}">
            <div class="accordion" id="myPostsAccordion">
                <div class="card">
                    <div class="card-header" id="myPostsHeading" role="button">
                        <div class="d-flex justify-content-between align-items-center" data-toggle="collapse" data-target="#myPosts" aria-expanded="false" aria-controls="myPosts">
                            <h3>My posts</h3>
                            <h5 style="margin-right: 10px">
                                <div class="row justify-content-center">
                                    <a href="${pageContext.request.contextPath}/create_post" class="badge badge-success">+</a>
                                    <span class="badge badge-primary badge-pill" style="margin-left: 15px">${my_posts.size()}</span>
                                </div>
                            </h5>
                        </div>
                    </div>

                    <div id="myPosts" class="collapse show" aria-labelledby="myPostsHeading" data-parent="#myPostsAccordion">
                        <div class="list-group">
                            <c:forEach items="${my_posts}" var="post">
                                <a href="post/${post.getUuid()}" class="list-group-item list-group-item-action">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">${post.getTitle()}</h5>
                                        <small>${post.createdAt}</small>
                                    </div>
                                </a>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <p>You don`t have any posts</p>
            <a href="create_post">It`s time to make some!</a>
        </c:otherwise>
    </c:choose>
</div>

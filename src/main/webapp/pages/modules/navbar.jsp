<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/${pageContext.request.contextPath}/home">
        <img src="https://pbs.twimg.com/profile_images/760081767013416960/fQMUDGFE_400x400.jpg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
        JSP
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <c:forEach items="${links}" var="link">
                <li class="nav-item">
                    <a class="nav-link" href='${pageContext.request.contextPath}/${link.href}'>${link.label}</a>
                </li>
            </c:forEach>
        </ul>
    </div>
</nav>
<style>
    <%@include file="../../styles/style.css" %>
</style>
<div class="posts_block">
    <c:choose>
        <c:when test="${posts.size() > 0}">
            <div class="accordion" id="postsAccordion">
                <div class="card">
                    <div class="card-header" id="postsHeading">
                        <div class="d-flex justify-content-between align-items-center" data-toggle="collapse" data-target="#posts" aria-expanded="false" aria-controls="posts">
                            <h3>Posts</h3>
                            <h5 style="margin-right: 10px">
                                    <span class="badge badge-primary badge-pill">${posts.size()}</span>
                            </h5>
                        </div>
                    </div>

                    <div id="posts" class="collapse show" aria-labelledby="postsHeading" data-parent="#postsAccordion">
                        <div class="list-group">
                            <c:forEach items="${posts}" var="post">
                                <a href="${pageContext.request.contextPath}/post/${post.getUuid()}" class="list-group-item list-group-item-action">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">${post.getTitle()}</h5>
                                        <small>${post.createdAt}</small>
                                    </div>
                                </a>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </c:when>
    </c:choose>
</div>

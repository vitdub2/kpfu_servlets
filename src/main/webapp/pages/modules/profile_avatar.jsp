<div id="profile_avatar_block">
    <c:choose>
        <c:when test="${avatar}">
            <img src="${pageContext.request.contextPath}/ajax/avatar?id=${user.id}" alt="Avatar"/>
        </c:when>
        <c:otherwise>
            <img src="${pageContext.request.contextPath}/files/default_avatar.png" alt="Avatar"/>
        </c:otherwise>
    </c:choose>
</div>

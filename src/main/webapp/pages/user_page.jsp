<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
    <%@ include file="modules/head.jsp"%>
</head>
<body>
    <%@ include file="modules/navbar.jsp" %>
    <script>
        const renderBtn = (isSubscribed) => {
            const wrapper = $("#btn-wrapper");
            if (isSubscribed) {
                wrapper.html(
                    '<button class="btn btn-danger" onclick="toggleSubscribe()">Unsubscribe</button>'
                )
            } else {
                wrapper.html(
                    '<button class="btn btn-success" onclick="toggleSubscribe()">Subscribe</button>'
                )
            }
        }

        const toggleSubscribe = () => {
            $.ajax({
                url: "${pageContext.request.contextPath}/ajax/subscription?id=${user.id}",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: (resp) => {
                    const isSubscribed = JSON.parse(resp);
                    renderBtn(isSubscribed);
                }
            })
        }
    </script>
    <main>
        <div class="container">
            <h4>${user.username}`s profile</h4>
            <div id="profile_avatar_block">
                <img src="${pageContext.request.contextPath}/ajax/avatar?id=${user.id}" alt="Avatar"/>
            </div>
            <div id="btn-wrapper">
                <div id="preloaded_btn">
                    <c:choose>
                        <c:when test="${subscribed}">
                            <button onclick="toggleSubscribe()" class="btn btn-danger">Unsubscribe</button>
                        </c:when>
                        <c:otherwise>
                            <button onclick="toggleSubscribe()" class="btn btn-success">Subscribe</button>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <%@ include file="modules/posts_block.jsp" %>
        </div>
    </main>
</body>
</html>

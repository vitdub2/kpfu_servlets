<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
    <%@ include file="modules/head.jsp"%>
</head>
<body>
<%@ include file="modules/navbar.jsp" %>
<main>
    <div class="container">
        <h2>My subscriptions</h2>
        <c:choose>
            <c:when test="${subs.size() > 0}">
                <ul class="list-group">
                    <c:forEach items="${subs}" var="user">
                        <a role="button" href="user/${user.id}" type="button" class="list-group-item list-group-item-action">
                            ${user.username}
                        </a>
                    </c:forEach>
                </ul>
            </c:when>
            <c:otherwise>
                <p>You don`t have any subscriptions</p>
                <a href="users">It`s time to make some!</a>
            </c:otherwise>
        </c:choose>
    </div>
</main>
</body>

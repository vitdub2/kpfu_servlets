<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
    <%@ include file="modules/head.jsp"%>
</head>
<body>
    <%@ include file="modules/navbar.jsp" %>
    <main>
        <div class="container">
            <h2>Sign Up</h2>
            <div id="form">
                <div class="form-group">
                    <label for="login-input">Login</label>
                    <input type="text" name="login" id="login-input" autocomplete="off" class="form-control">
                    <div id="login-feedback" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label for="username-input">Username</label>
                    <input type="text" name="username" id="username-input" autocomplete="off" class="form-control">
                    <div id="username-feedback" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label for="first_name-input">First name</label>
                    <input type="text" name="first_name" id="first_name-input" autocomplete="off" class="form-control">
                    <div id="first_name-feedback" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label for="last_name-input">Last name</label>
                    <input type="text" name="last_name" id="last_name-input" autocomplete="off" class="form-control">
                    <div id="last_name-feedback" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label for="password-input">Password</label>
                    <input type="password" name="password" id="password-input" autocomplete="off" class="form-control">
                    <div id="password-feedback" class="invalid-feedback"></div>
                </div>
                <button onclick="upload()" class="btn btn-primary" id="submit-btn">
                    Submit
                </button>
            </div>
        </div>
    </main>
    <script>
        let formState = {}

        const input_names = ["login", "first_name", "last_name", "username", "password"];

        input_names.forEach((name) => {
            formState = {
                ...formState,
                [name]: $("input[name='" + name +"']").val()
            }
        });

        input_names.forEach((name) => {
            $("input[name='" + name +"']").change((e) => {
                formState[name] = e.target.value;
            })
        })

        const renderInvalidForm = (errors) => {
            $('input').removeClass('is-invalid');
            $('.invalid-feedback').html('');
            const keys = Object.keys(errors);
            if (keys && keys.length > 0) {
                keys.forEach(key => {
                    $("#" + key + "-input").addClass('is-invalid');
                    $("#" + key + "-feedback").html(errors[key]);
                })
            } else {
                window.location.href = "${pageContext.request.contextPath}/profile";
            }
        }

        const upload = () => {
            $.ajax({
                url: "${pageContext.request.contextPath}/sign_up",
                type: "POST",
                data: JSON.stringify(formState),
                success: (errors) => {
                    renderInvalidForm(errors);
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    </script>
</body>
</html>

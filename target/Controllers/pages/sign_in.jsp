<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sign in</title>
    <%@ include file="modules/head.jsp"%>
</head>
<body>
    <%@ include file="modules/navbar.jsp" %>
    <main>
        <div class="container">
            <h2>Sign In</h2>
            <div id="form-wrapper">
                <div class="form-group">
                    <label for="login-input">Login</label>
                    <input type="text" name="login" id="login-input" class="form-control">
                </div>
                <div class="form-group" id="password-form-group">
                    <label for="password-input">Password</label>
                    <input type="password" name="password" id="password-input" class="form-control">
                </div>
                <button onclick="upload()" class="btn btn-primary" id="submit-btn">
                    Submit
                </button>
            </div>
            <div class="alert alert-danger form-alert" id="errors" role="alert">

            </div>
        </div>
    </main>
</body>
<script>
    const wrapper = $('#errors');
    wrapper.hide();

    const renderInvalidForm = (errors) => {
        if (errors.length) {
            wrapper.show();
            wrapper.html('');
            let html = ''
            for (let error of errors) {
                html += `<li>` + error + `</li>`
            }
            wrapper.html(`<ul>` + html + `</ul>`);
        } else {
            window.location.href = "${pageContext.request.contextPath}/profile";
        }
    }

    const upload = () => {
        let data = {
            login: $("#login-input").val(),
            password: $("#password-input").val(),
        }
        $.ajax({
            url: "${pageContext.request.contextPath}/sign_in",
            type: "POST",
            data: JSON.stringify(data),
            success: (resp) => {
                renderInvalidForm(resp);
            },
            cache: false,
            contentType: false,
            processData: false
        })
    }
</script>
</html>
